# Memory Lane
My capstone project for TCNJ's IMM program, 2013

## Abstract

*Memory Lane* is a study on how human interaction in a video game experience is affected by peripherals.  Players will enter an amnesiac's dreams in order to save his life.  They will experience his dreams as they would in reality through real-world gestures and vocal input, as processed by the motion-sensing, microphone-enabled Kinect sensor.  

##  Well...

That's what it would do if I managed to implement all the functionality.  As it is, Memory Lane is currently a proof-of-concept without direct processing of voice or hyper-accurate gestures; those have been simulated. The tracking tech I used - the Omek Motion Toolkit paired with the Kinect - has since gone defunct.  So, if you'd like to expand on the idea with new motion tech (like, say, the Vive), then by all means go ahead and fork. I'll be attempting the same when time and finances permit.